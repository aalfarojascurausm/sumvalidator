
function twoNumberSum(array, targetSum){
    for (let i = 0; i < array.length; i++){
        for (let j = 0; j < array.length; j++)
            if (array[i] + array[j] == targetSum) return [array[i], array[j]]
    }
    return []
}

const array = [-9, -5, -2, 1, 4, 7, 10]
const targetSum = 2

const result = twoNumberSum(array, targetSum)
console.log(result.length ? `Resultado: [${result[0]}, ${result[1]}]` : "No hay coincidencias")